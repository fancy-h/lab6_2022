<?php

namespace App\Controllers;

class Hero extends BaseController {

    public function getIndex() {
        // connect to the model
        $places = new \App\Models\Skiers();
        // retrieve all the records
        $records = $places->findAll();
        // get a template parser
        $parser = \Config\Services::parser();
        // tell it about the substitions
        return $parser->setData(['records' => $records])
        // and have it render the template with those
        ->render('skierslist');
    }
    public function getShowme($id)
        {
               // connect to the model
        $places = new \App\Models\Skiers();
        // retrieve all the records
        $record = $places->find($id);
        // get a template parser
        $parser = \Config\Services::parser();
        // tell it about the substitions
        return $parser->setData($record)
        // and have it render the template with those
        ->render('oneskier');
    }
}
