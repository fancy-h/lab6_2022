<?php
namespace App\Models;

/*
 * Mock travel destination data.
 * Note that we don't have to extend CodeIgniter's model for now
 */

class Skiers {
    //mock data : an array of records
    protected $data = [
        '1' => [
            'id' => 1,
            'name' => 'Gu Ailing',
            'city' => 'Beijing',
            'favourite food'=>'Dumpling, Beijing Roast Duck, Pie',
            'specialty'=>'Freestyle Sking',
            'hobby'=>'Running, Play basketball, Horse riding',
            'image' => '1.jpg',
        ],
        '2' => [
            'id' => 2,
            'name' => 'Xu Mengtao',
            'city' => 'Anshang',
            'favourite food'=>'Dumpling,Fish,Pineapple',
            'specialty'=>'Freestyle Sking',
            'hobby'=>'Singing, Dancing',
            'image' => '2.jpg',
        ],
        '3' => [
            'id' => 3,
            'name' => 'Oliver Davis',
            'city' => 'London',
            'favourite food'=>'Hamburger,croissant,pudding',
            'specialty'=>'Freestyle Sking',
            'hobby'=>'Swimming, Play football',
            'image' => '3.jpg',
        ],
        '4' => [
            'id' => 4,
            'name' => 'Nick Page',
            'city' => 'NewYork',
            'favourite food'=>'baguette, sandwich, cauliflower',
            'specialty'=>'Freestyle Sking',
            'hobby'=>'Riding, Diving',
            'image' => '4.jpg',
        ],
        '5' => [
            'id' => 5,
            'name' => 'Bradley Wilson',
            'city' => 'Seattle',
            'favourite food'=>'broccoli, beef, pizza',
            'specialty'=>'Freestyle Sking',
            'hobby'=>'Play baseball, Swimming',
            'image' => '5.jpg',
        ],
    ];

    public function findAll() {
        return $this->data;
    }

    public function find($id = null) {
        if (!empty($id) && isset($this->data[$id])) {
            return $this->data[$id];
        }
        return null;
    }

}
